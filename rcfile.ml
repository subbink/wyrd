(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)

(* rcfile.ml
 * This file includes everything associated with processing the wyrdrc file.
 * In particular, this includes a number of hashtables used to store the
 * bindings of curses keypresses to calendar operations.
 * Adapted from rcfile code in Orpie, a curses RPN calculator. *)

open Genlex
open Curses


module PairSet = Set.Make (
   struct
      type t      = int * int
      let compare = compare
   end
)

exception Config_failure of string
let config_failwith s = raise (Config_failure s)

type command_t = | ScrollUp | ScrollDown | NextDay | PrevDay
                 | NextWeek | PrevWeek | NextMonth | PrevMonth
                 | Home | Zoom | Edit | EditAny | NewTimed | NewUntimed
                 | NewTimedDialog | NewUntimedDialog | SwitchWindow
                 | SearchNext | BeginSearch | Quit | ViewReminders
                 | ScrollDescUp | ScrollDescDown | Refresh
                 | ViewAllReminders | ViewWeek | ViewMonth
                 | NextReminder | ViewKeybindings | CopyReminder
                 | PasteReminder | PasteReminderDialog
                 | CutReminder | Goto | QuickEvent
                 | NewGenReminder of int | NewGenReminderDialog of int

type entry_operation_t = | EntryComplete | EntryBackspace | EntryExit

type operation_t = CommandOp of command_t | EntryOp of entry_operation_t

type colorable_object_t = | Help | Timed_default | Timed_current | Untimed_reminder
                          | Timed_date | Selection_info | Description | Status
                          | Calendar_labels | Calendar_level1 | Calendar_level2
                          | Calendar_level3 | Calendar_today | Left_divider | Right_divider
                          | Timed_reminder1 | Timed_reminder2 | Timed_reminder3
                          | Timed_reminder4


(* These hashtables store conversions between curses keys and the operations
 * they are associated with. *)
let table_key_command = Hashtbl.create 20
let table_command_key = Hashtbl.create 20

let table_key_entry = Hashtbl.create 20
let table_entry_key = Hashtbl.create 20

let table_commandstr_command = Hashtbl.create 30
let table_command_commandstr = Hashtbl.create 30

(* Default Remind command *)
let remind_command = ref "remind"
(* Default reminders file *)
let reminders_file = ref "$HOME/.reminders"
(* Default editing command strings *)
let (edit_old_command, edit_new_command, edit_any_command) =
   if Utility.valid_env "EDITOR" then (
      ref {|$EDITOR +%line% "%file%"|},
      ref {|$EDITOR +999999 "%file%"|},
      ref {|$EDITOR "%file%"|}
   ) else (
      ref {|vi +%line% "%file%"|},
      ref {|vi -c '$' "%file%"|},
      ref {|vi "%file%"|}
   )
(* Default pager command *)
let pager_command = ref (
   if Utility.valid_env "PAGER" then "$PAGER"
   else if Sys.command "less -c 2>/dev/null" = 0 then "less -c"
   else "less"
)
(* Default zoom level *)
let default_zoom = ref 0
(* Default new reminder templates *)
let timed_template   = ref "REM %monname% %mday% %year% AT %hour%:%min% DURATION 1:00 MSG "
let untimed_template = ref "REM %monname% %mday% %year% MSG "
let template0        = ref @@ Some ("REM %wdayname% AT %hour%:%min% DURATION 1:00 MSG ")
let template1        = ref @@ Some ("REM %wdayname% MSG ")
let template2        = ref @@ Some ("REM %mday% AT %hour%:%min% DURATION 1:00 MSG ")
let template3        = ref @@ Some ("REM %mday% MSG ")
let template4        = ref None
let template5        = ref None
let template6        = ref None
let template7        = ref None
let template8        = ref None
let template9        = ref None
(* algorithm to use for counting busy-ness *)
let busy_algorithm = ref 1
(* number of minutes to assume an untimed reminder requires *)
let untimed_duration = ref 60.
(* Default thresholds for calendar colorization *)
let busy_level1 = ref 1
let busy_level2 = ref 3
let busy_level3 = ref 5
let busy_level4 = ref 7
(* First day of the week? *)
let week_starts_monday = ref false
(* 12/24 hour time selection *)
let schedule_12_hour    = ref false
let selection_12_hour   = ref true
let status_12_hour      = ref true
let description_12_hour = ref true
(* Center the schedule on the cursor? *)
let center_cursor = ref false
(* "jump to" date syntax is big-endian? *)
let goto_big_endian = ref true
(* "quick add" date syntax uses US conventions? *)
let quick_date_US = ref true
(* print week numbers? *)
let number_weeks = ref false
(* home is "sticky"? *)
let home_sticky = ref true
(* width of calendar/untimed reminder windows *)
let untimed_window_width = ref 40
(* trigger advance warning of reminders? *)
let advance_warning = ref false
(* render untimed reminders in bold? *)
let untimed_bold = ref true
(* render reminders in color? *)
let reminder_colors = ref None

(* List of included rc files *)
let included_rcfiles : (string list) ref = ref []

(* Temporary hash tables for sorting out the color palette *)
let color_table         = Hashtbl.create 20
let inverse_color_table = Hashtbl.create 20
(* Final hash table that maps from object to color_pair index *)
let object_palette      = Hashtbl.create 20


(* Turn colors on and off *)
let color_on win obj =
   try
      let color_index = Hashtbl.find object_palette obj in
      wattron win (A.color_pair color_index)
   with Not_found ->
      ()

let color_off win obj =
   try
      let color_index = Hashtbl.find object_palette obj in
      wattroff win (A.color_pair color_index)
   with Not_found ->
      ()


let command_of_key key =
   Hashtbl.find table_key_command key
let key_of_command command =
   Hashtbl.find table_command_key command

let entry_of_key key =
   Hashtbl.find table_key_entry key
let key_of_entry entry =
   Hashtbl.find table_entry_key entry


let decode_single_key_string key_string =
   let decode_alias str =
      match str with
      |"<esc>"       -> 27
      |"<tab>"       -> 9
      |"<enter>"     -> Key.enter
      |"<return>"    -> 10
      |"<insert>"    -> Key.ic
      |"<delete>"    -> Key.dc
      |"<home>"      -> Key.home
      |"<end>"       -> Key.end_
      |"<pageup>"    -> Key.ppage
      |"<pagedown>"  -> Key.npage
      |"<space>"     -> 32
      |"<backspace>" -> Key.backspace
      |"<left>"      -> Key.left
      |"<right>"     -> Key.right
      |"<up>"        -> Key.up
      |"<down>"      -> Key.down
      |"<f1>"        -> (Key.f 1)
      |"<f2>"        -> (Key.f 2)
      |"<f3>"        -> (Key.f 3)
      |"<f4>"        -> (Key.f 4)
      |"<f5>"        -> (Key.f 5)
      |"<f6>"        -> (Key.f 6)
      |"<f7>"        -> (Key.f 7)
      |"<f8>"        -> (Key.f 8)
      |"<f9>"        -> (Key.f 9)
      |"<f10>"       -> (Key.f 10)
      |"<f11>"       -> (Key.f 11)
      |"<f12>"       -> (Key.f 12)
      |_             ->
         if String.length key_string = 1 then
            int_of_char str.[0]
         else
            config_failwith ({|Unrecognized key "|} ^ str ^ {|"|})
   in
   (* This regexp is used to extract the ctrl and meta characters from a string
    * representing a keypress.
    * It matches \\M\\C or \\C\\M or \\C or \\M (or no such characters) followed
    * by an arbitrary string. *)
   let cm_re = Str.regexp
   {|^\(\(\\M\\C\|\\C\\M\)\|\(\\M\)\|\(\\C\)\)?\(<.+>\|.\)|}
   in
   if Str.string_match cm_re key_string 0 then
      let has_meta_ctrl =
         try let _ = Str.matched_group 2 key_string in true
         with Not_found -> false
      and has_meta =
         try let _  = Str.matched_group 3 key_string in true
         with Not_found -> false
      and has_ctrl =
         try let _ = Str.matched_group 4 key_string in true
         with Not_found -> false
      and main_key = Str.matched_group 5 key_string in
      if has_meta_ctrl then
         if String.length main_key = 1 then
            let uc_main_key = String.uppercase_ascii main_key in
            let mc_chtype = ((int_of_char uc_main_key.[0]) + 64) in
            let mc_str = "M-C-" ^ uc_main_key in
            (mc_chtype, mc_str)
         else
            config_failwith ({|Cannot apply \\M\\C to key "|} ^ main_key ^ "\";\n" ^
                       "octal notation might let you accomplish this.")
      else if has_meta then
         if String.length main_key = 1 then
            let m_chtype = ((int_of_char main_key.[0]) + 128) in
            let m_str = "M-" ^ main_key in
            (m_chtype, m_str)
         else
            config_failwith ({|Cannot apply \\M to key "|} ^ main_key ^ "\";\n" ^
                       "octal notation might let you accomplish this.")
      else if has_ctrl then
         if String.length main_key = 1 then
            let uc_main_key = String.uppercase_ascii main_key in
            let c_chtype = ((int_of_char uc_main_key.[0]) - 64) in
            let c_str = "C-" ^ uc_main_key in
            (c_chtype, c_str)
         else
            config_failwith ({|Cannot apply \\C to key "|} ^ main_key ^ "\";\n" ^
                       "octal notation might let you accomplish this.")
      else
         let octal_regex = Str.regexp "^0o" in
         try
            let _ = Str.search_forward octal_regex key_string 0 in
            ((int_of_string key_string), ("\\" ^ Str.string_after key_string
            2))
         with
            _ -> ((decode_alias main_key), main_key)
   else
      config_failwith ("Unable to match binding string with standard regular expression.")



(* Register a key binding.  This adds hash table entries for translation
 * between curses chtypes and commands (in both directions). *)
let register_binding_internal k k_string op =
   match op with
   |CommandOp x ->
      Hashtbl.add table_key_command k x;
      Hashtbl.add table_command_key x k_string
   |EntryOp x ->
      Hashtbl.add table_key_entry k x;
      Hashtbl.add table_entry_key x k_string


(* convenience routine for previous *)
let register_binding key_string op =
   (* given a string that represents a character, find the associated
    * curses chtype *)
   let k, string_rep = decode_single_key_string key_string in
   register_binding_internal k string_rep op


(* unregister a binding *)
let unregister_binding key_string =
   let k, _ = decode_single_key_string key_string in
   begin try
      let op = Hashtbl.find table_key_command k in
      Hashtbl.remove table_key_command k;
      Hashtbl.remove table_command_key op
   with Not_found ->
      ()
   end;
   begin try
      let op = Hashtbl.find table_key_entry k in
      Hashtbl.remove table_key_entry k;
      Hashtbl.remove table_entry_key op
   with Not_found ->
      ()
   end


let () =
   let commands_list = [
      ("scroll_up"               , CommandOp ScrollUp);
      ("scroll_down"             , CommandOp ScrollDown);
      ("next_day"                , CommandOp NextDay);
      ("previous_day"            , CommandOp PrevDay);
      ("next_week"               , CommandOp NextWeek);
      ("previous_week"           , CommandOp PrevWeek);
      ("next_month"              , CommandOp NextMonth);
      ("previous_month"          , CommandOp PrevMonth);
      ("home"                    , CommandOp Home);
      ("goto"                    , CommandOp Goto);
      ("zoom"                    , CommandOp Zoom);
      ("edit"                    , CommandOp Edit);
      ("edit_any"                , CommandOp EditAny);
      ("copy"                    , CommandOp CopyReminder);
      ("cut"                     , CommandOp CutReminder);
      ("paste"                   , CommandOp PasteReminder);
      ("paste_dialog"            , CommandOp PasteReminderDialog);
      ("scroll_description_up"   , CommandOp ScrollDescUp);
      ("scroll_description_down" , CommandOp ScrollDescDown);
      ("quick_add"               , CommandOp QuickEvent);
      ("new_timed"               , CommandOp NewTimed);
      ("new_timed_dialog"        , CommandOp NewTimedDialog);
      ("new_untimed"             , CommandOp NewUntimed);
      ("new_untimed_dialog"      , CommandOp NewUntimedDialog);
      ("new_template0"           , CommandOp (NewGenReminder 0));
      ("new_template0_dialog"    , CommandOp (NewGenReminderDialog 0));
      ("new_template1"           , CommandOp (NewGenReminder 1));
      ("new_template1_dialog"    , CommandOp (NewGenReminderDialog 1));
      ("new_template2"           , CommandOp (NewGenReminder 2));
      ("new_template2_dialog"    , CommandOp (NewGenReminderDialog 2));
      ("new_template3"           , CommandOp (NewGenReminder 3));
      ("new_template3_dialog"    , CommandOp (NewGenReminderDialog 3));
      ("new_template4"           , CommandOp (NewGenReminder 4));
      ("new_template4_dialog"    , CommandOp (NewGenReminderDialog 4));
      ("new_template5"           , CommandOp (NewGenReminder 5));
      ("new_template5_dialog"    , CommandOp (NewGenReminderDialog 5));
      ("new_template6"           , CommandOp (NewGenReminder 6));
      ("new_template6_dialog"    , CommandOp (NewGenReminderDialog 6));
      ("new_template7"           , CommandOp (NewGenReminder 7));
      ("new_template7_dialog"    , CommandOp (NewGenReminderDialog 7));
      ("new_template8"           , CommandOp (NewGenReminder 8));
      ("new_template8_dialog"    , CommandOp (NewGenReminderDialog 8));
      ("new_template9"           , CommandOp (NewGenReminder 9));
      ("new_template9_dialog"    , CommandOp (NewGenReminderDialog 9));
      ("switch_window"           , CommandOp SwitchWindow);
      ("search_next"             , CommandOp SearchNext);
      ("begin_search"            , CommandOp BeginSearch);
      ("next_reminder"           , CommandOp NextReminder);
      ("view_remind"             , CommandOp ViewReminders);
      ("view_remind_all"         , CommandOp ViewAllReminders);
      ("view_week"               , CommandOp ViewWeek);
      ("view_month"              , CommandOp ViewMonth);
      ("refresh"                 , CommandOp Refresh );
      ("help"                    , CommandOp ViewKeybindings);
      ("entry_complete"          , EntryOp EntryComplete);
      ("entry_backspace"         , EntryOp EntryBackspace);
      ("entry_cancel"            , EntryOp EntryExit);
      ("quit"                    , CommandOp Quit)
   ] in
   let create_translation ((commandstr, operation) : string * operation_t) =
      Hashtbl.add table_commandstr_command commandstr operation;
      Hashtbl.add table_command_commandstr operation commandstr
   in
   List.iter create_translation commands_list


(* translate a command string to the command type it represents *)
let operation_of_string command_str =
   Hashtbl.find table_commandstr_command command_str

(* translate a command to a string representation *)
let string_of_operation op =
   Hashtbl.find table_command_commandstr op


(* Parse a line from a configuration file.  This operates on a stream
 * corresponding to a non-empty line from the file.  It will match commands
 * of the form
 *    bind key command
 * where 'key' is either a quoted string containing a key specifier or an octal
 * key representation of the form \xxx (unquoted), and multiple_keys is a quoted
 * string containing a number of keypresses to simulate.
 *)
let[@alert "-deprecated"] parse_line line_stream =
   (* Convenience function for 'set' keyword *)
   let parse_set variable_str variable coersion error =
      begin match Stream.next line_stream with
      | Ident "=" ->
         begin match Stream.next line_stream with
         | String ss ->
            begin try
               variable := coersion ss
            with _ ->
               config_failwith (error ^ {|"set |} ^ variable_str ^ {| = "|})
            end
         | _ ->
            config_failwith (error ^ {|"set |} ^ variable_str ^ {| = "|})
         end
      | _ ->
         config_failwith ({|Expected "=" after "set |} ^ variable_str ^ {|"|})
      end
   in
   (* Convenience function for 'color' keyword *)
   let parse_color obj_str obj =
      let foreground =
         begin match Stream.next line_stream with
         | Ident "black"   -> Color.black
         | Ident "red"     -> Color.red
         | Ident "green"   -> Color.green
         | Ident "yellow"  -> Color.yellow
         | Ident "blue"    -> Color.blue
         | Ident "magenta" -> Color.magenta
         | Ident "cyan"    -> Color.cyan
         | Ident "white"   -> Color.white
         | Ident "default" -> ~- 1
         | _ ->
            config_failwith ({|Expected a foreground color after "set |} ^ obj_str)
         end
      in
      let background =
         begin match Stream.next line_stream with
         | Ident "black"   -> Color.black
         | Ident "red"     -> Color.red
         | Ident "green"   -> Color.green
         | Ident "yellow"  -> Color.yellow
         | Ident "blue"    -> Color.blue
         | Ident "magenta" -> Color.magenta
         | Ident "cyan"    -> Color.cyan
         | Ident "white"   -> Color.white
         | Ident "default" -> ~- 1
         | _ ->
            config_failwith ({|Expected a background color after "set |} ^ obj_str)
         end
      in
      Hashtbl.replace color_table obj (foreground, background)
   in
   (* Parsing begins here *)
   match Stream.next line_stream with
   | Kwd "include" ->
      begin match Stream.next line_stream with
      | String include_file ->
         included_rcfiles := include_file :: !included_rcfiles
      | _ ->
         config_failwith ({|Expected a filename string after "include"|})
      end
   | Kwd "bind" ->
      let bind_key key =
         begin match Stream.next line_stream with
         | Ident command_str ->
            begin try
               let command = operation_of_string command_str in
               register_binding key command
            with Not_found ->
               config_failwith ({|Unrecognized command name "|} ^ command_str ^ {|"|})
            end
         | _ ->
            config_failwith ({|Expected a command name after "bind |} ^ key ^ {|"|})
         end
      in
      begin match Stream.next line_stream with
      | String k ->
         bind_key k
      | Ident "\\" ->
         begin match Stream.next line_stream with
         | Int octal_int ->
            begin
               try
                  let octal_digits = "0o" ^ (string_of_int octal_int) in
                  bind_key octal_digits
               with
                  (Failure _ (* int_of_string *) ) -> config_failwith {|Expected octal digits after "\"|}
            end
         | _  ->
            config_failwith {|Expected octal digits after "\"|}
         end
      | _ ->
         config_failwith {|Expected a key string after keyword "bind"|}
      end
   | Kwd "unbind" ->
      begin match Stream.next line_stream with
      | String k ->
         unregister_binding k
      | _ ->
         config_failwith {|Expected a key string after keyword "unbind"|}
      end
   | Kwd "set" ->
      let bool variable_str variable =
         parse_set variable_str variable
            bool_of_string "Expected a boolean string after "
      in
      let cmd variable_str variable =
         parse_set variable_str variable
            (fun x -> x) "Expected a command string after "
      in
      let file variable_str variable =
         parse_set variable_str variable
            (fun x -> x) "Expected a filename string after "
      in
      let float variable_str variable =
         parse_set variable_str variable
            float_of_string "Expected a float string after "
      in
      let template variable_str variable =
         parse_set variable_str variable
            (fun x -> x) "Expected a template string after "
      in
      let template_opt variable_str variable =
         parse_set variable_str variable
            (fun x -> Some x) "Expected a template string after "
      in
      let int variable_str variable =
         parse_set variable_str variable
            int_of_string "Expected an integral string after "
      in
      let range ~first ~last variable_str variable =
         parse_set variable_str variable
            (fun x -> let i = int_of_string x in
               let () = assert (i >= first && i <= last) in i)
            (Printf.sprintf
               "Expected an integral string of %d to %d after "
               first last)
      in
      begin match Stream.next line_stream with
      | Ident "remind_command" -> cmd "remind_command" remind_command
      | Ident "reminders_file" -> file "reminders_file" reminders_file
      | Ident "edit_old_command" -> cmd "edit_old_command" edit_old_command
      | Ident "edit_new_command" -> cmd "edit_new_command" edit_new_command
      | Ident "edit_any_command" -> cmd "edit_any_command" edit_any_command
      | Ident "pager_command" -> cmd "pager_command" pager_command
      | Ident "default_zoom" -> range ~first:0 ~last:2 "default_zoom" default_zoom
      | Ident "timed_template" -> template "timed_template" timed_template
      | Ident "untimed_template" -> template "untimed_template" untimed_template
      | Ident "template0" -> template_opt "template0" template0
      | Ident "template1" -> template_opt "template1" template1
      | Ident "template2" -> template_opt "template2" template2
      | Ident "template3" -> template_opt "template3" template3
      | Ident "template4" -> template_opt "template4" template4
      | Ident "template5" -> template_opt "template5" template5
      | Ident "template6" -> template_opt "template6" template6
      | Ident "template7" -> template_opt "template7" template7
      | Ident "template8" -> template_opt "template8" template8
      | Ident "template9" -> template_opt "template9" template9
      | Ident "busy_algorithm" -> int "busy_algorithm" busy_algorithm
      | Ident "untimed_duration" -> float "untimed_duration" untimed_duration
      | Ident "busy_level1" -> int "busy_level1" busy_level1
      | Ident "busy_level2" -> int "busy_level2" busy_level2
      | Ident "busy_level3" -> int "busy_level3" busy_level3
      | Ident "busy_level4" -> int "busy_level4" busy_level4
      | Ident "week_starts_monday" -> bool "week_starts_monday" week_starts_monday
      | Ident "schedule_12_hour" -> bool "schedule_12_hour" schedule_12_hour
      | Ident "selection_12_hour" -> bool "selection_12_hour" selection_12_hour
      | Ident "status_12_hour" -> bool "status_12_hour" status_12_hour
      | Ident "description_12_hour" -> bool "description_12_hour" description_12_hour
      | Ident "center_cursor" -> bool "center_cursor" center_cursor
      | Ident "goto_big_endian" -> bool "goto_big_endian" goto_big_endian
      | Ident "quick_date_US" -> bool "quick_date_US" quick_date_US
      | Ident "number_weeks" -> bool "number_weeks" number_weeks
      | Ident "home_sticky" -> bool "home_sticky" home_sticky
      | Ident "untimed_window_width" -> int "untimed_window_width" untimed_window_width
      | Ident "advance_warning" -> bool "advance_warning" advance_warning
      | Ident "untimed_bold" -> bool "untimed_bold" untimed_bold
      | Ident "reminder_colors" ->
         parse_set "reminder_colors" reminder_colors (function
            | "auto"  -> None
            | "true"  -> Some true
            | "false" -> Some false
            | _       -> assert false)
            "Expected a value of either: auto, true, or false"
      | _ ->
         config_failwith {|Unmatched variable name after "set"|}
      end
   | Kwd "color" ->
      begin match Stream.next line_stream with
      | Ident "help"             -> parse_color "help" Help
      | Ident "timed_default"    -> parse_color "timed_default" Timed_default
      | Ident "timed_current"    -> parse_color "timed_current" Timed_current
      | Ident "timed_reminder"   -> config_failwith
             {|"timed_reminder" has been deprecated.  Please use "timed_reminder1".|}
      | Ident "untimed_reminder" -> parse_color "untimed_reminder" Untimed_reminder
      | Ident "timed_date"       -> parse_color "timed_date" Timed_date
      | Ident "selection_info"   -> parse_color "selection_info" Selection_info
      | Ident "description"      -> parse_color "description" Description
      | Ident "status"           -> parse_color "status" Status
      | Ident "calendar_labels"  -> parse_color "calendar_labels" Calendar_labels
      | Ident "calendar_level1"  -> parse_color "calendar_level1" Calendar_level1
      | Ident "calendar_level2"  -> parse_color "calendar_level2" Calendar_level2
      | Ident "calendar_level3"  -> parse_color "calendar_level3" Calendar_level3
      | Ident "calendar_selection" ->
         begin
         Printf.fprintf stderr "Warning: colorable object \"calendar_selection\" has been ";
         Printf.fprintf stderr "deprecated.\nPlease remove this reference from the wyrdrc file.\n"
         end
      | Ident "calendar_today"   -> parse_color "calendar_today" Calendar_today
      | Ident "left_divider"     -> parse_color "left_divider" Left_divider
      | Ident "right_divider"    -> parse_color "right_divider" Right_divider
      | Ident "timed_reminder1"  -> parse_color "timed_reminder1" Timed_reminder1
      | Ident "timed_reminder2"  -> parse_color "timed_reminder2" Timed_reminder2
      | Ident "timed_reminder3"  -> parse_color "timed_reminder3" Timed_reminder3
      | Ident "timed_reminder4"  -> parse_color "timed_reminder4" Timed_reminder4
      | _ -> raise Stream.Failure
      end
   | Kwd "#" ->
      ()
   | _ ->
      config_failwith "Expected a keyword at start of line"


let reminder_colors = function
   | `Check_support -> Option.is_none !reminder_colors
   | `Is_enabled    -> Option.value !reminder_colors ~default:false
   | `Set bool      -> reminder_colors := Some bool; bool


(* open included rcfile *)
let open_rcfile file =
   try (Utility.expand_open_in_ascii file, file)
   with Sys_error _error_str -> config_failwith
      ({|Could not open configuration file "|} ^ file ^ {|".|})


(* validate the color table to make sure that the number of defined colors is legal *)
let validate_colors () =
   (* form a Set of all color pairs, and an inverse mapping from color pair to objects *)
   let initial_palette =
      let process_entry key pair colors =
         Hashtbl.add inverse_color_table pair key;
         PairSet.add pair colors
      in
      Hashtbl.fold process_entry color_table PairSet.empty
   in
   (* start killing off color pairs as necessary to fit within color_pairs limit *)
   let rec reduce_colors palette =
      if PairSet.cardinal palette > pred (color_pairs ()) then begin
         (* find and remove the color pair with fewest objects assigned *)
         let min_objects = ref 100000
         and best_pair   = ref (-1, -1) in
         let find_best pair =
            let obj_list = Hashtbl.find_all inverse_color_table pair in
            if List.length obj_list < !min_objects then begin
               min_objects := List.length obj_list;
               best_pair   := pair
            end else
               ()
         in
         PairSet.iter find_best palette;
         (* the color pair needs to be removed from two hashtables and the palette set *)
         let obj_list = Hashtbl.find_all inverse_color_table !best_pair in
         List.iter (Hashtbl.remove color_table) obj_list;
         Hashtbl.remove inverse_color_table !best_pair;
         reduce_colors (PairSet.remove !best_pair palette)
      end else
         palette
   in
   let register_color_pair pair n =
      assert (init_pair n (fst pair) (snd pair));
      let f obj = Hashtbl.add object_palette obj n in
      List.iter f (Hashtbl.find_all inverse_color_table pair);
      succ n
   in
   let _ = PairSet.fold register_color_pair (reduce_colors initial_palette) 1 in ()



let rec process_rcfile (config_stream, rcfile_filename) =
   let line_lexer line =
      make_lexer
         ["include"; "bind"; "unbind"; "set"; "color"; "#"]
      (Stream.of_string line)[@alert "-deprecated"]
   in
   let empty_regexp = Str.regexp "^[\t ]*$" in
   let line_num = ref 0 in
   try
      while true do
         line_num := succ !line_num;
         let line_string = input_line config_stream in
         (* Printf.fprintf stderr "read line %2d: %s\n" !line_num line_string;
         flush stderr; *)
         if Str.string_match empty_regexp line_string 0 then
            (* do nothing on an empty line *)
            ()
         else
            try
               let line_stream = line_lexer line_string in
               parse_line line_stream;
               (* process any included rcfiles as they are encountered *)
               begin match !included_rcfiles with
               |[] -> ()
               |head :: tail ->
                  included_rcfiles := tail;
                  process_rcfile (open_rcfile head)
               end
            with
               |Config_failure s ->
                  (let error_str = Printf.sprintf "Syntax error on line %d of \"%s\": %s"
                  !line_num rcfile_filename s in
                  failwith error_str)
               |Stream.Failure ->
                  failwith (Printf.sprintf "Syntax error on line %d of \"%s\""
                  !line_num rcfile_filename)

      done
   with End_of_file ->
      begin
         close_in config_stream
      end


(* default keybindings and colors, these will be overwritten if provided in wyrdrc *)
let () =
   register_binding "j"           (operation_of_string "scroll_down");
   register_binding "<down>"      (operation_of_string "scroll_down");
   register_binding "k"           (operation_of_string "scroll_up");
   register_binding "<up>"        (operation_of_string "scroll_up");
   register_binding "h"           (operation_of_string "switch_window");
   register_binding "l"           (operation_of_string "switch_window");
   register_binding "<left>"      (operation_of_string "switch_window");
   register_binding "<right>"     (operation_of_string "switch_window");
   register_binding "<pageup>"    (operation_of_string "previous_day");
   register_binding "4"           (operation_of_string "previous_day");
   register_binding "<"           (operation_of_string "previous_day");
   register_binding "H"           (operation_of_string "previous_day");
   register_binding "<pagedown>"  (operation_of_string "next_day");
   register_binding "6"           (operation_of_string "next_day");
   register_binding ">"           (operation_of_string "next_day");
   register_binding "L"           (operation_of_string "next_day");
   register_binding "8"           (operation_of_string "previous_week");
   register_binding "["           (operation_of_string "previous_week");
   register_binding "K"           (operation_of_string "previous_week");
   register_binding "2"           (operation_of_string "next_week");
   register_binding "]"           (operation_of_string "next_week");
   register_binding "J"           (operation_of_string "next_week");
   register_binding "{"           (operation_of_string "previous_month");
   register_binding "}"           (operation_of_string "next_month");
   register_binding "<home>"      (operation_of_string "home");
   register_binding "g"           (operation_of_string "goto");
   register_binding "z"           (operation_of_string "zoom");
   register_binding "<return>"    (operation_of_string "edit");
   register_binding "<enter>"     (operation_of_string "edit");
   register_binding "e"           (operation_of_string "edit_any");
   register_binding "y"           (operation_of_string "copy");
   register_binding "X"           (operation_of_string "cut");
   register_binding "p"           (operation_of_string "paste");
   register_binding "P"           (operation_of_string "paste_dialog");
   register_binding "d"           (operation_of_string "scroll_description_up");
   register_binding "D"           (operation_of_string "scroll_description_down");
   register_binding "q"           (operation_of_string "quick_add");
   register_binding "t"           (operation_of_string "new_timed");
   register_binding "T"           (operation_of_string "new_timed_dialog");
   register_binding "u"           (operation_of_string "new_untimed");
   register_binding "U"           (operation_of_string "new_untimed_dialog");
   register_binding "w"           (operation_of_string "new_template0");
   register_binding "W"           (operation_of_string "new_template1");
   register_binding "m"           (operation_of_string "new_template2");
   register_binding "M"           (operation_of_string "new_template3");
   register_binding "n"           (operation_of_string "search_next");
   register_binding "/"           (operation_of_string "begin_search");
   register_binding "<tab>"       (operation_of_string "next_reminder");
   register_binding "r"           (operation_of_string "view_remind");
   register_binding "R"           (operation_of_string "view_remind_all");
   register_binding "c"           (operation_of_string "view_week");
   register_binding "C"           (operation_of_string "view_month");
   register_binding "?"           (operation_of_string "help");
   register_binding "\\Cl"        (operation_of_string "refresh");
   register_binding "Q"           (operation_of_string "quit");
   register_binding "<return>"    (operation_of_string "entry_complete");
   register_binding "<enter>"     (operation_of_string "entry_complete");
   register_binding "<backspace>" (operation_of_string "entry_backspace");
   register_binding "<esc>"       (operation_of_string "entry_cancel");
   register_binding "\\Cg"        (operation_of_string "entry_cancel")


let () =
   let open Curses.Color in
   let color obj foreground background =
      Hashtbl.replace color_table obj (foreground, background)
   in
   color Help               green blue;
   color Timed_default      white black;
   color Timed_current      white red;
   color Timed_reminder1    yellow blue;
   color Timed_reminder2    white red;
   color Timed_reminder3    white green;
   color Timed_reminder4    yellow magenta;
   color Untimed_reminder   white black;
   color Timed_date         cyan black;
   color Selection_info     green blue;
   color Description        white black;
   color Status             green blue;
   color Calendar_labels    white black;
   color Calendar_level1    white black;
   color Calendar_level2    blue black;
   color Calendar_level3    magenta black;
   color Calendar_today     white red;
   color Left_divider       cyan blue;
   color Right_divider      cyan blue
