/*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 */


/* OCaml binding for setlocale(), which is required to kick ncurses
 * into rendering non-ASCII chars. */

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>

#include <locale.h>

value ml_setlocale(value category, value setting)
{
   CAMLparam2(category, setting);
   CAMLlocal1(result);

   char* loc = setlocale(Int_val(category), String_val(setting));
   if (!loc) {
      result = Val_int(0); // None
   } else {
      result = caml_alloc_boxed(caml_copy_string(loc)); // Some string
   }
   CAMLreturn(result);
}
