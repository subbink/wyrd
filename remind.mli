(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


exception Occurrence_not_found

type timed_rem_t = {
   tr_start      : float;
   tr_end        : float;
   tr_duration   : float;
   tr_cal        : string;
   tr_msg        : string;
   tr_filename   : string;
   tr_linenum    : string;
   tr_has_weight : bool;
   tr_rgb        : (int * int * int) option;
}

type untimed_rem_t = {
   ur_start      : float;
   ur_cal        : string;
   ur_msg        : string;
   ur_filename   : string;
   ur_linenum    : string;
   ur_has_weight : bool;
   ur_rgb        : (int * int * int) option;
}

type three_month_rem_t = {
   curr_timestamp : float;
   prev_timed     : timed_rem_t list array;
   curr_timed     : timed_rem_t list array;
   next_timed     : timed_rem_t list array;
   all_timed      : timed_rem_t list array;
   prev_untimed   : untimed_rem_t list;
   curr_untimed   : untimed_rem_t list;
   next_untimed   : untimed_rem_t list;
   all_untimed    : untimed_rem_t list;
   curr_counts    : int array;
   curr_cal       : Cal.t;
   remind_error   : string
}

val merge_timed : timed_rem_t list array -> timed_rem_t list

val create_three_month : ?suppress_advwarn:bool -> float -> three_month_rem_t

val update_reminders : three_month_rem_t -> float -> three_month_rem_t

val find_next : Str.regexp -> float -> float

val get_all_remfiles : unit -> string * string list

val get_untimed_reminders_for_day : untimed_rem_t list -> float -> untimed_rem_t list

val get_remfile_mtimes : unit -> float Interface.SMap.t
