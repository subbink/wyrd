(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


val join_path : string -> string -> string

val expand_file : string -> string
val expand_open_in_ascii : string -> in_channel

val string_of_tm_mon : int -> string
val full_string_of_tm_mon : int -> string
val short_string_of_tm_wday : int -> string
val string_of_tm_wday : int -> string
val full_string_of_tm_wday : int -> string
val empty_tm : Unix.tm

val strip : string -> string
val read_all_shell_command_output : string -> string list * string list

val utf8_len : string -> int
val utf8_string_before : string -> int -> string
val utf8_string_after : string -> int -> string

val valid_env : string -> bool
