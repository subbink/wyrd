(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


type screen_t = {
   stdscr       : Curses.window;
   lines        : int;
   cols         : int;
   help_win     : Curses.window;
   hw_cols      : int;
   timed_win    : Curses.window;
   tw_lines     : int;
   tw_cols      : int;
   calendar_win : Curses.window;
   cw_lines     : int;
   cw_cols      : int;
   untimed_win  : Curses.window;
   uw_lines     : int;
   uw_cols      : int;
   msg_win      : Curses.window;
   mw_lines     : int;
   mw_cols      : int;
   err_win      : Curses.window;
   ew_lines     : int;
   ew_cols      : int
}

type zoom_t = Hour | HalfHour | QuarterHour
type side_t = Left | Right

type timed_lineinfo_t = {
   tl_filename : string;
   tl_linenum  : string;
   tl_timestr  : string;
   tl_cal      : string;
   tl_msg      : string;
   tl_start    : float
}

type untimed_lineinfo_t = {
   ul_filename : string;
   ul_linenum  : string;
   ul_msg      : string
}

type extended_mode_t = ExtendedSearch | ExtendedGoto | ExtendedQuick
type entry_mode_t = Normal | Extended of extended_mode_t

module SMap : sig
   include Map.S
      with type key := string
end

type interface_state_t = {
   version           : string;              (* program version string *)
   scr               : screen_t;            (* curses screen with two or three subwindows *)
   run_wyrd          : bool;                (* exit when run_wyrd becomes false *)
   top_timestamp     : float;               (* controls what portion of the schedule is viewable *)
   top_untimed       : int;                 (* controls what portion of untimed reminders are viewable *)
   top_desc          : int;                 (* controls what portion of the reminder descriptions are viewable *)
   selected_side     : side_t;              (* controls which window has the focus *)
   left_selection    : int;                 (* controls which element of the left window is selected *)
   right_selection   : int;                 (* controls which element of the right window is selected *)
   zoom_level        : zoom_t;              (* controls the resolution of the timed window *)
   timed_lineinfo    : timed_lineinfo_t list array;     (* information about the lines of the timed reminder window *)
   untimed_lineinfo  : untimed_lineinfo_t option array; (* same as above, for untimed window *)
   len_untimed       : int;                 (* number of entries in the untimed reminders list *)
   remfile_mtimes    : float SMap.t;        (* for each remfile, maps filename to stat mtime *)
   search_regex      : Str.regexp;          (* most recent search string *)
   extended_input    : string;              (* buffer to hold search/goto/quick event info *)
   entry_mode        : entry_mode_t;        (* decides which mode the interface is in *)
   last_timed_refresh: float;               (* the last time the timed window had a complete refresh *)
   rem_buffer        : string;              (* buffer that acts as a clipboard for REM strings *)
   track_home        : bool;                (* true if cursor position should "stick" to current time *)
   resize_failed_win : Curses.window option (* if a resize fails, this holds a window pointer for an error msg. *)
}

val round_time : zoom_t -> Unix.tm -> Unix.tm

val make : string -> screen_t -> interface_state_t

val time_inc : interface_state_t -> float
val time_inc_min : interface_state_t -> int

val timestamp_of_line : interface_state_t -> int -> float
