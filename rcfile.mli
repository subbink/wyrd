(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


val config_failwith : string -> 'a

type command_t = | ScrollUp | ScrollDown | NextDay | PrevDay
                 | NextWeek | PrevWeek | NextMonth | PrevMonth
                 | Home | Zoom | Edit | EditAny | NewTimed | NewUntimed
                 | NewTimedDialog | NewUntimedDialog | SwitchWindow
                 | SearchNext | BeginSearch | Quit | ViewReminders
                 | ScrollDescUp | ScrollDescDown | Refresh
                 | ViewAllReminders | ViewWeek | ViewMonth
                 | NextReminder | ViewKeybindings | CopyReminder
                 | PasteReminder | PasteReminderDialog
                 | CutReminder | Goto | QuickEvent
                 | NewGenReminder of int | NewGenReminderDialog of int

type entry_operation_t = | EntryComplete | EntryBackspace | EntryExit

type operation_t = CommandOp of command_t | EntryOp of entry_operation_t

type colorable_object_t = | Help | Timed_default | Timed_current | Untimed_reminder
                          | Timed_date | Selection_info | Description | Status
                          | Calendar_labels | Calendar_level1 | Calendar_level2
                          | Calendar_level3 | Calendar_today | Left_divider | Right_divider
                          | Timed_reminder1 | Timed_reminder2 | Timed_reminder3
                          | Timed_reminder4

val table_command_key : (command_t, string) Hashtbl.t
val table_entry_key : (entry_operation_t, string) Hashtbl.t
val table_commandstr_command : (string, operation_t) Hashtbl.t
val remind_command : string ref
val reminders_file : string ref
val edit_old_command : string ref
val edit_new_command : string ref
val edit_any_command : string ref
val pager_command : string ref
val default_zoom : int ref
val timed_template : string ref
val untimed_template : string ref
val template0 : string option ref
val template1 : string option ref
val template2 : string option ref
val template3 : string option ref
val template4 : string option ref
val template5 : string option ref
val template6 : string option ref
val template7 : string option ref
val template8 : string option ref
val template9 : string option ref
val busy_algorithm : int ref
val untimed_duration : float ref
val busy_level1 : int ref
val busy_level2 : int ref
val busy_level3 : int ref
val busy_level4 : int ref
val week_starts_monday : bool ref
val schedule_12_hour : bool ref
val selection_12_hour : bool ref
val status_12_hour : bool ref
val description_12_hour : bool ref
val center_cursor : bool ref
val goto_big_endian : bool ref
val quick_date_US : bool ref
val number_weeks : bool ref
val home_sticky : bool ref
val untimed_window_width : int ref
val advance_warning : bool ref
val untimed_bold : bool ref
val reminder_colors : [< `Check_support | `Is_enabled | `Set of bool ] -> bool
val color_table : (colorable_object_t, int * int) Hashtbl.t
val object_palette : (colorable_object_t, int) Hashtbl.t
val color_on : Curses.window -> colorable_object_t -> unit
val color_off : Curses.window -> colorable_object_t -> unit
val command_of_key : int -> command_t
val key_of_command : command_t -> string
val entry_of_key : int -> entry_operation_t
val register_binding : string -> operation_t -> unit
val operation_of_string : string -> operation_t
val validate_colors : unit -> unit
val process_rcfile : in_channel * string -> unit
