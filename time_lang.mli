(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


exception Event_parse_error of string

type event_t = Timed of (Unix.tm * Unix.tm) | Untimed of Unix.tm

val parse_natural_language_event : string -> event_t * string
