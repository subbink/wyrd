PREFIX ?= /usr/local
MANDIR ?= $(PREFIX)/share/man
INSTALL ?= install

all: manual/wyrd.1 manual/wyrd.7 manual/wyrdrc.5

manual: all manual.pdf manual/index.html

page-manual.odoc: manual.mld
	odoc compile -I . --child page-wyrdrc --child page-wyrd $<

page-wyrd.odoc: wyrd.mld page-manual.odoc
	odoc compile -I . --parent page-manual $<

page-wyrdrc.odoc: wyrdrc.mld page-manual.odoc
	odoc compile -I . --parent page-manual $<

page-manual.odocl: page-manual.odoc page-wyrd.odoc page-wyrdrc.odocl
	odoc link -I . $<

page-wyrd.odocl: page-wyrd.odoc
	odoc link -I . $<

page-wyrdrc.odocl: page-wyrdrc.odoc
	odoc link -I . $<

manual/index.html: page-manual.odocl manual/wyrd.html manual/wyrdrc.html manual/odoc.css
	odoc html -o . $<
	sed -i'' 's@\.\./@@g' $@

manual/wyrd.html: page-wyrd.odocl manual/odoc.css
	odoc html -o . $<
	sed -i'' 's@\.\./@@g' $@

manual/wyrdrc.html: page-wyrdrc.odocl manual/odoc.css
	odoc html -o . $<
	sed -i'' 's@\.\./@@g' $@

manual/odoc.css:
	odoc support-files -o manual

manual/manual.tex: page-manual.odocl
	odoc latex -o manual $<
	sed -i'' -e '2,4 d' $@

manual/wyrd.tex: page-wyrd.odocl
	odoc latex -o . $<

manual/wyrdrc.tex: page-wyrdrc.odocl
	odoc latex -o . $<
	sed -i'' -e 's/page-manual-leaf-page-wyrdrc-//g' $@

manual.pdf: manual.tex manual/manual.tex manual/wyrd.tex manual/wyrdrc.tex
	latexmk -pdflua $<

manual/wyrd.1: page-wyrd.odocl footer.man
	odoc man -o . $<
	cat footer.man >> manual/wyrd.3o
	sed -e 's/^.TH wyrd 3/.TH wyrd 1/' -e 's/OCaml Library/a console calendar application/' \
	    -e 's/^manual..wyrd/wyrd - a text-based front-end to remind(1), a sophisticated calendar and alarm program./' \
	    -e '/.SH Documentation/d' -e 's/.in 3/.in 0/' \
	    -e 's/MANPAGES/wyrd(7), wyrdrc(5), remind(1)/' manual/wyrd.3o > $@
	rm manual/wyrd.3o

manual/wyrd.7: page-manual.odocl footer.man
	odoc man -o manual $<
	cat footer.man >> manual/manual.3o
	sed -e 's/^.TH manual 3/.TH wyrd 7/' -e 's/OCaml Library/a console calendar application/' \
	    -e 's/^manual$$/wyrd - the user manual for wyrd(1), an ncurses interface to remind(1)./' \
	    -e 's/MANPAGES/wyrd(1), wyrdrc(5), remind(1)/' manual/manual.3o > $@
	rm manual/manual.3o

manual/wyrdrc.5: page-wyrdrc.odocl footer.man
	odoc man -o . $<
	cat footer.man >> manual/wyrdrc.3o
	sed -e 's/^.TH wyrdrc 3/.TH wyrdrc 5/' -e 's/OCaml Library/Wyrd configuration file/' \
	    -e 's/^manual..wyrdrc/wyrdrc - the configuration textfile for the wyrd(1) console calendar application./' \
	    -e 's/MANPAGES/wyrd(1), wyrd(7), remind(1)/' manual/wyrdrc.3o > $@
	rm manual/wyrdrc.3o

install: all
	$(INSTALL) -d $(DESTDIR)$(MANDIR)/man1 \
		      $(DESTDIR)$(MANDIR)/man7 \
		      $(DESTDIR)$(MANDIR)/man5
	$(INSTALL) -m 644 manual/wyrd.1   $(DESTDIR)$(MANDIR)/man1/
	$(INSTALL) -m 644 manual/wyrd.7   $(DESTDIR)$(MANDIR)/man7/
	$(INSTALL) -m 644 manual/wyrdrc.5 $(DESTDIR)$(MANDIR)/man5/

uninstall:
	rm -f $(DESTDIR)$(MANDIR)/man1/wyrd.1 \
	      $(DESTDIR)$(MANDIR)/man7/wyrd.7 \
	      $(DESTDIR)$(MANDIR)/man5/wyrdrc.5

distclean:
	latexmk -c || true
	rm -vf manual/*.tex

clean: distclean
	rm -Rvf manual manual.pdf

.INTERMEDIATE: page-wyrd.odoc page-wyrd.odocl page-manual.odoc page-manual.odocl page-wyrdrc.odoc page-wyrdrc.odocl
.PHONY: all manual install uninstall distclean clean
